import java.awt.Color;

public class GameShot extends GameObject {

	public GameShot(float x, float y, int width, int height, int velx, int vely, Color color) {
        super(x, y, width, height, velx, vely, color);
		this.collide = new GameCollide(this, true, true);
    }
    
	public void move(long DiffTime){
		float time = DiffTime/1000.0f;
		x += velx * time;
		y += vely * time;
	}
	
    public void onCollision(String side){
		if(side == "left" || side == "right") {
			velx = velx * -1;
		}
		if(side == "top" || side == "bot") {
			vely = vely * -1;
		}
    }

	
	public void update(long DiffTime) {
		
		super.update(DiffTime);
		
	}
}