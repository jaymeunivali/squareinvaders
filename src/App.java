import java.awt.BorderLayout;
import javax.swing.JFrame;

public class App  {
    	
	public static Game game = null;
    public static Panel panel = null;

	public App() {
	} // end of GamePanel()

	public static void main(String args[]) {
		Panel panel = new Panel(game);

		// create a JFrame to hold the timer test JPanel
		JFrame app = new JFrame("Game Core");
		app.getContentPane().add(panel, BorderLayout.CENTER);
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		app.pack();
		app.setResizable(false);
		app.setVisible(true);
	} // end of main()

} // end of GamePanel class