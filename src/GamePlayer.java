import java.awt.Color;
import java.awt.event.MouseEvent;

public class GamePlayer extends GameObject {
		public boolean UP, DOWN, LEFT, RIGHT, FIRE;
		public GameShot shot;
		
		public GamePlayer(float x, float y, int width, int height, int velx, int vely, Color color) {
			super(x, y, width, height, velx, vely, color);
			this.collide = new GameCollide(this, true, true);
		}

		public void fire(){
			// float degree = getAngle(mouseX, mouseY);
			shot = new GameShot(x + (width / 2), y, 10, 10, 0, -100, Color.RED);
		}

		
	
		public float getAngle(int mouseX, int mouseY) {
			float angle = (float) Math.toDegrees(Math.atan2(mouseX - y, mouseY - x));
		
			if(angle < 0){
				angle += 360;
			}
		
			return angle;
		}
    

		public void move(long DiffTime){
			if(UP) {
				y -= vely * DiffTime/1000.0f;
			} 
			if(DOWN) {
				y += vely * DiffTime/1000.0f;
			} 
			if(RIGHT) {
				x += velx * DiffTime/1000.0f;
			} 
			if(LEFT) {
				x -= velx * DiffTime/1000.0f;
			}
		}
		
		public void update(long DiffTime) {
			
			
			super.update(DiffTime);


			
		}
		
	}
