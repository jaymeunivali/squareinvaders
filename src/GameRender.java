import java.awt.Graphics2D;
import java.awt.Color;

public class GameRender {
	public int PWIDTH;
	public int PHEIGHT;
    public GameRender(int PWIDTH, int PHEIGHT){
		this.PWIDTH = PWIDTH;
		this.PHEIGHT = PHEIGHT;
	}
    
	public void render(Graphics2D dbg, int FPS)
	// draw the current frame to an image buffer
	{
		// clear the background
		dbg.setColor(Color.black);
		dbg.fillRect(0, 0, this.PWIDTH, this.PHEIGHT);

		for (int i = 0; i < GameObject.list.size(); i++) {
			GameObject.list.get(i).render(dbg);
		}

		// draw game elements
		dbg.setColor(Color.WHITE);
		dbg.drawString("FPS: " + FPS, 20, 20);

	} // end of render()
}