import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;

public class Panel extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	public static Panel instance = null;
	private Thread animator; // for the animation
	
	Game game;

	public Panel(Game game) {
		instance = this;
		this.game = new Game();
	
		setBackground(Color.white); // white background
		setPreferredSize(new Dimension(Game.PWIDTH, Game.PHEIGHT));

		setLayout(new BorderLayout());
		add(Game.gui, BorderLayout.CENTER);

		setFocusable(true); // create game components
		requestFocus(); // JPanel now receives key events

	} // end of GamePanel()

	public void addNotify() {
		super.addNotify(); // creates the peer
		startGame(); // start the thread
	}

	private void startGame()
	// initialise and start the thread
	{
		if (animator == null || !Game.running) {
			animator = new Thread(this);
			animator.start();
		}
	} // end of startGame()

	public void stopGame()
	// called by the user to stop execution
	{
		Game.running = false;
	}

	public void run()
	/* Repeatedly update, render, sleep */
	{
		Game.run();
	}

} // end of GamePanel class
