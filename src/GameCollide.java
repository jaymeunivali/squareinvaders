import java.util.ArrayList;

public class GameCollide {
    
    public boolean withBounds;
    public boolean withObjects;
    
    public GameObject body;
    public GameCollide(GameObject body, boolean withBounds, boolean withObjects) {
        this.body = body;
        this.withBounds = withBounds;
        this.withObjects = withObjects;
    }

    public void bounds() {
            if(this.body.x + this.body.width > Game.PWIDTH) {
                this.body.onCollision("right");
            }
            
            if(this.body.x < 0) {
                this.body.onCollision("left");
            }
            
            if(this.body.y + this.body.height > Game.PHEIGHT) {
                this.body.onCollision("bot");
            }
            
            if(this.body.y < 0) {
                this.body.onCollision("top");
            }
    }

    public boolean isNear(float point1, float point2) {
        if(point1 - point2 > 1 || point1 - point2 > -1){
            return true;
        }
        return false;
    }
    
    public void collisior(GameObject collisor){

        float xLeft = this.body.x;
        float xRight = this.body.x + this.body.width;
        float yTop = this.body.y;
        float yBot = this.body.y + this.body.height;
        float width = this.body.width;
        float height = this.body.height;
        float _xLeft = collisor.x;
        float _xRight = collisor.x + collisor.width;
        float _yTop = collisor.y;
        float _yBot = collisor.y + collisor.height;
        float _width = collisor.width;
        float _height = collisor.height;
        // if (
        //     xLeft < _xRight
        //     && xRight > _xLeft
        //     && yTop < _yBot
        //     && yBot > _yTop
        // ) {
        //     System.out.println("collision");
        // } else {
        //     System.out.println("no collision");
        // }
    }

    public void objects(){
        for(int i = 0; i < GameObject.list.size(); i++) {
            if (this.body != GameObject.list.get(i)) this.collisior(GameObject.list.get(i));
        }
    }

    public void check(){
        if(this.withBounds) this.bounds();
        if(this.withObjects) this.objects();
    }

}